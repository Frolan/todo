var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var http = require('http');
var bodyParser = require("body-parser");

/*var router = express.Router();

router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());*/

var app = express();
var db = require('./db');

//var AuthController = require('./auth/AuthController');
//app.use('/auth', AuthController);

//var UserController = require('./user/UserController');
//app.use('/users', UserController);

app.use(require('connect-livereload')());

app.use(express.static(__dirname + '/public'));

var VerifyToken = require('./auth/VerifyToken');
//var User = require('./user/User');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

var urlencodedParser = bodyParser.urlencoded({extended: false});

var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
var bcrypt = require('bcryptjs');
var config = require('./config'); // get config file

var mongoose = require('mongoose');  

var taskSchema = new mongoose.Schema({
  id: Number,
  done: Boolean,
  task: String
});

var UserSchema = new mongoose.Schema({  
  name: String,
  password: String,
  tasks: [taskSchema]
});

var User = mongoose.model('User', UserSchema);

// Запрос на регистрацию
app.post('/registration', function(req, res) {

  var hashedPassword = bcrypt.hashSync(req.body.password, 8);

  console.log(req.body.name);
  console.log(hashedPassword);

  User.findOne({ name: req.body.name }, function (err, user) {
    if (err) return res.status(500).send('Error on the server.');
    if (user) return res.status(404).send('User already exist.');

    else{
      User.create({
        name : req.body.name,
        password : hashedPassword,
        tasks: []
      }, 
      function (err, user) {
        if (err){
          console.log(err);
           return res.status(500).send("There was a problem registering the user`.");
        }
    
        // if user is registered without errors
        // create a token
        var token = jwt.sign({ id: user._id }, config.secret, {
          expiresIn: 86400 // expires in 24 hours
        });
        console.log(token);
        res.status(200).send({ auth: true, token: token });
      });
    }
  });
});

// Запрос на авторизацию
app.post('/login', function(req, res) {

  User.findOne({ name: req.body.name }, function (err, user) {
    if (err) return res.status(500).send('Error on the server.');
    if (!user) return res.status(404).send('No user found.');
    
    // check if the password is valid
    var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
    if (!passwordIsValid) return res.status(401).send({ auth: false, token: null });

    // if user is found and password is valid
    // create a token
    var token = jwt.sign({ id: user._id }, config.secret, {
      expiresIn: 86400 // expires in 24 hours
    });

    // return the information including token as JSON
    console.log('login');
    res.status(200).json({ auth: true, token: token });
  });
});

app.get('/logout', function(req, res) {
  res.status(200).send({ auth: false, token: null });
});

// Запрос на обновление данных
app.post('/update', function(req, res) {
  var tasks = req.body.tasks;
  var name = req.body.name;
  User.updateOne({ name: name }, { tasks: tasks }, function (err, doc){
      if (err) {
          res.json({ok: false});
      } else if (doc.nModified != 0) {
          console.log(doc);
          res.json({ ok: true });
      }
  });
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
