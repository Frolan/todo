const gulp = require("gulp");
const path = require("path");
const debug = require("gulp-debug");
const browserSync = require("browser-sync").create();

const publicDir = path.join(__dirname, "public");
const appDir = "src";

const webpack = require('webpack');
const gutil = require('gulp-util');
const notifier = require('node-notifier');
const webpackConfig = require('./webpack.config.js');
const server = require('gulp-express');
const statsLog = { // для красивых логов в консоли
  colors: true,
  reasons: true
};

// Задача serve запускает сервер из нашего файла server.js
gulp.task('serve', function(done) {
    server.run(['./bin/www']);
    done();
  });

  gulp.task('browser-sync-init', function(done) {
    browserSync.init({
      proxy: `http://localhost:3000`,
      port: '3001'
    });
    done();
  });


gulp.task("build-html", function () {
  return gulp
    .src(`${appDir}/**/*.html`, {
      since: gulp.lastRun("build-html")
    })
    .pipe(debug({ title: "build-html" }))
    .pipe(gulp.dest(publicDir));
});

gulp.task("build-js", (done) => {
  // run webpack
  // webpack имеет свой собственный API, поэтому мы его можем подключать в своих NodeJS приложениях
  // webpack импортируется как обычная функция, которая первым аргументом получает конфиг из webpack.config.js
  // второй аргумент это колбэк, который вызывается по результатам работы сборки webpack
  webpack(webpackConfig, onComplete);
  function onComplete(error, stats) {
    if (error) { // Что-то невероятное
      onError(error);
    } else if (stats.hasErrors()) { // ошибки в самой сборке, к примеру "не удалось найти модуль по заданному пути"
      onError(stats.toString(statsLog));
    } else {
      onSuccess(stats.toString(statsLog));
    }
  }

  // Функции onError и onSuccess выводят в отформатированном виде информацию пользователю
  // Каждая функция должна вызвать колбэк done, который сигнализирует о завершении
  // асинхронной задачи build-js, что обязательно требует gulp
  function onError(error) {
    let formatedError = new gutil.PluginError('webpack', error);
    notifier.notify({ // чисто чтобы сразу узнать об ошибке
      title: `Error: ${formatedError.plugin}`,
      message: formatedError.message
    });
    gutil.log('[webpack]', error);
    done();
  }
  function onSuccess(detailInfo) {
    gutil.log('[webpack]', detailInfo);
    done();
  }
});
  
  gulp.task("default", gulp.series("build-html", "build-js", "serve", "browser-sync-init", function () {
	gulp.watch([`${appDir}/**/*.html`], gulp.series("build-html"));
  gulp
    .watch(`${publicDir}/**/*.*`)
    .on("change", path => browserSync.reload(path))
    .on("add", path => browserSync.reload(path));
}));
