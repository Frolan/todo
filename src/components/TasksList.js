import React, {Component} from 'react';
import styleTask from './Task.css';
import style from './TasksList.css';
import Task from "./Task";
import ReactDOM from 'react-dom'

export default class TaskList extends Component {
    state = {
        tasks: [],
        id: 0
    }

    render(){
        console.log(this.state.tasks);

        const listTasks = this.state.tasks.map((task) => task);
        return (
            <div className={style.taskList}>
            <input type="button" className={styleTask.btnStyle} onClick={this.addTask} value="Add"/>
                {listTasks}
            </div>
        )
    }

    removeTask = (e) =>{
        const idTaskRemove = e.currentTarget.parentNode.id;
        const newTasks = [];
        for(var i = 0; i < this.state.tasks.length; i++){
            if(this.state.tasks[i].key !== idTaskRemove)
                newTasks.push(this.state.tasks[i]);
        }
        this.setState({tasks: newTasks});
    }

    addTask = () =>{
        const newTasks = this.state.tasks;
        newTasks.push(
            <div key={++this.state.id} id={this.state.id} className={style.task}>
                <Task key={++this.state.id}/> 
                <input key={++this.state.id} type="button" className={styleTask.btnRemoveStyle} onClick={this.removeTask} value="Remove"/>
            </div>
        );

        this.setState({
            tasks: newTasks
        });
    }

    updateTasks = () =>{
        fetch('/update', {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({ 
                login: 'Netty', 
                tasks: tasks 
            })
          }).then(res => {
            return res.json();
          }).then(data => {
            console.log(data);
            if (data.ok == false) {
              alert("Не удалось обновить данные");
            }
          }).catch(error => {
            console.log(error);
          });
        }
    }
