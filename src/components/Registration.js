import React, {Component} from 'react';
import styleTask from './Task.css';
import style from './TasksList.css';

export default class Registration extends Component {
    render(){
        return (
            <div className={style.taskList}>
                <div className={styleTask.fieldStyle}>
                    <label htmlFor="login">Login:</label><input type="text" id="login" className={styleTask.inputStyle}/>
                </div>
                <div className={styleTask.fieldStyle}>
                    <label htmlFor="password">Password:</label><input type="password" id="password" className={styleTask.inputStyle}/>
                </div>
                <input type="button" className={styleTask.btnStyle} onClick={this.handleClick} value="Create"/>        
            </div>
        )
    }

    handleClick = () => {
        fetch('/registration', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                name: document.getElementById('login').value,
                password: document.getElementById('password').value,
            }),
        }).then(function(res) {
            if(res.ok){
               return res.json();
            }
            else
                alert('Warning! This acccount already exist!')
        }).then(function(data) { 
            if(data){          
                localStorage.setItem('token', data.token);
                alert('Account create successful!');
            }
        }).catch(function(err) {
            alert(err.message);
        });
    }
}