import React, {Component} from 'react';
import style from './App.css';
import TasksList from "./TasksList";
import Registration from "./Registration";
import Login from "./Login";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

export default class App extends Component {
    render(){
        return (
            <div>
                <header className={style.appHeader}>
                    <h1>
                        ToDo List
                    </h1>
                </header>
                <Router>
                    <div>
                        <ul className={style.menu}>
                            <li><Link className={style.link} to="/todo">My List</Link></li>
                            <li><Link className={style.link} to="/registration">Sign Up</Link></li>
                            <li><Link className={style.link} to="/login">Sign In</Link></li>
                            <li><Link className={style.link} onClick={this.handleClick} to="/login">Log Out</Link></li>
                        </ul>

                        <div className={style.appBody}>
                            <Route exact path="/todo" component={TasksList} />
                            <Route path="/registration" component={Registration} />
                            <Route path="/login" component={Login} />
                        </div>
                    </div>
                </Router>
            </div>
        )
    }

    handleClick = () => {
        fetch('/logout', {
            method: 'GET',
            headers,
            body,
        }).then(function(res) {
            if(res.ok){
               alert('Log out successful!');
               localStorage.setItem('token', null);
            }
        }).catch(function(err) {
            alert(err.message);
        });
    }
}