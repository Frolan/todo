import React, {Component} from 'react';
import style from './Task.css';

export default class Task extends Component {
    state = {
        isDone: false 
    };

    render(){
        return (
            <div className={style.fieldStyle}>
                <textarea id="textField" className={this.state.isDone ? style.inputDoneStyle : style.inputStyle} disabled = {this.state.isDone}/>
                <input type="button" className={this.state.isDone ? style.btnDoneStyle : style.btnStyle} onClick={this.handleClick} value={this.state.isDone ? "Not Done" : "Done"}/>
            </div>
        )
    }

    handleClick = () => {
        this.setState({
            isDone: !this.state.isDone
        })
    }
}

