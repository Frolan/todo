import React, {Component} from 'react';
import styleTask from './Task.css';
import style from './TasksList.css';

export default class Login extends Component {
    render(){
        return (
            <div className={style.taskList}>
                <div className={styleTask.fieldStyle}>
                    <label htmlFor="login">Login:</label><input type="text" id="login" className={styleTask.inputStyle}/>
                </div>
                <div className={styleTask.fieldStyle}>
                    <label htmlFor="password">Password:</label><input type="password" id="password" className={styleTask.inputStyle}/>
                </div>
                <input type="button" className={styleTask.btnStyle} onClick={this.handleClick} value="Log In"/>
            </div>
        )
    }

    handleClick = () => {
        fetch('/login', {
            method: 'POST',
            headers: {
                Accept: 'application/json', 
                'Content-Type': 'application/json', 
            },
            body: JSON.stringify({
                name: document.getElementById('login').value,
                password: document.getElementById('password').value,
            }),
        }).then(function(res) {
            if(res.ok){
               return res.json();
            }
        }).then(function(data) { 
            if(data){ 
                console.log(data);         
                localStorage.setItem('token', data.token);
                alert('Log in successful!');
            }
        }).catch(function(err) {
            alert(err.message);
        });
    }
}